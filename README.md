## Revision 1

### Reviewer #2:

This study uses field measurements of geomorphic features and index/process equation values to complete a comprehensive statistical analysis with the goal to produce regression equations to predict stream bank erosion rates for the coastal plain of Alabama, Florida, and Georgia. By the author's statements, the main contribution of this work is to develop a tool for resource managers while secondarily showcasing an approach to develop the best multivariate regression models and presenting a new method to assess vegetation cover effects on banks. The best model developed contains five co-variables and predicts 54% of the variance in bank erosion rates (this can lead to big errors in field assessments if error sources are not understood). However, the model improves to explaining 65% of the variance when bank volume is used (i.e., bank erosion rate is multiplied by the bank height; caution here since bank ht is also in co-variable/BEHI). Two geomorphic variables, bank slope or angle and radius of curvature are in the final model as well as two vegetation resistance variables (tree biomass and bank understory cover). The fifth variable is the BEHI index value from Rosgen that has been used to predict bank erosion rates in many other watersheds, not always with good results. Nevertheless, the BEHI index is an important variable in this study.

However, I am curious about the internal co-linearity of BEHI with the other variables since the Index is calculated with bank angle and vegetation indicators and therefore their effects are replicated in the equation with three other variables (AGBw, Sb, PC%).

* The reviewer brings up a good point about potential collinearity between these variables. We plotted these 3 variables against BEHI in R to investigate. Visually, there are no (AGBw, PC), or weak at best (Sb) correlations. The correlation coefficients bear this out. For AGBw, Sb, and Pc, respectively, R=(-0.09, -0.26, -0.16). These results have been added to the text (line 469 ff.)

Overall, the approach used by these authors to develop these equations is more statistical rather than geomorphological. While the statistical rationale for modeling is justified, its basis for use to develop assessment tools, advance science, and evaluate unique regional/coastal plain factors is not so clear. While the statistical scope has been justified, the contribution of this study to the goals of Catena requires that some additional aspects be included and discussed to address the applications of this work to managers and fluvial scientists. At first I was concerned that this study was more of the same, just building models from a pile of variables to see what comes out? But the data set and analytical output of this study has the potential to be a contribution for publication. Further, the authors do provide a good to-the-point review of the literature and develop their analytical approach from it that can be useful to other researchers. I suggest some ways below to improve the manuscript in relation to my concerns. I realize that the paper is already 30+ pages and maybe some of the statistical background and discussion can be shortened to add space for these improvements.

* Statistical background has been shortened somewhat.

A big, often overlooked, problem with developing regression models for applications to geomorphic assessments, is that different workers will evaluate field setting and measure channel variables differently. So one team will apply their biased values to the original model, expecting the same results/justification as in the first published reference, but never really testing the assumption. For this study, I cannot reproduce the field sampling procedures the authors used since the methods are not described to any great depth (except for their new root density method). A revised manuscript needs a better description of methods used to measure channel morphology including sampling grids and transect arrays and number of points measured. If these procedures are available under separate/accessible cover, that is fine, but the critical aspects of the method should be included or summarized. For example, for the present study we do not know how channel width and bank ht was determined from field data, how many transects were averaged... only one collected? How representative for use in other external models such as to predict bankfull Q? Were riffle or pool or bend transects used and how averaged overall? At the least, the basic sampling array and transect/grid layouts for field sampling need to be detailed and justified for use in external equations and management tools like BEHI.

* Description of transects and bank morphology measurements has been added to the Materials and Methods section.
* We are also able to cite another paper describing how BEHI and NBS data were collected for this study (line 235).

What types of streams are the authors studying here? A review of channel morph and distribution of key variables is needed. Maybe add a table that reports average and range of site values for channel width, depth, bed slope, etc. for different watershed size/area classes (small to large) and maybe geology classes too (if variable). A histogram with some spatial themes about site location/Ad in it for bank erosion rates and bank heights or volumes would be helpful too.

* Added paragraph in Study Area section.
* Bank geology was largely sandy alluvial sediment (added to line 118) and did not vary much throughout the coastal plain study area.
* Table 2 created with min., max., mean, and standard dev. of each variable. Hopefully this will convey enough information about the studied streambanks.
* We are open to creating another figure, especially in the supplementary materials, if the reviewers/edtiors think more information should be added to the manuscript.

Prediction errors/residuals need to be evaluated over Ad, channel width, and watershed/land use? Presently, I am not confident that errors are geographically random and this assumption, if wrong, would negate the application of this tool for managers and gloss over the broader contributions to understanding bank erosion processes. Are there systematic patterns in errors based on network scale or geology/land use? Do errors increase proportionately with Ad or w, are they random, or is model performance worse for smaller compared to larger watersheds? Plotting residuals with symbols denoting different watershed location or geology could help to simply evaluate visually. Maybe,floodplain soil types from USDA soil survey layers could help to evaluate if bank composition is similar (enough)or if errors are more pronounced with one floodplain soil series compared to another?

* Added Figure S1 in the supplementary materials that shows the following. Residuals are not correlated to Ad, channel width, Fsd, Fcy, or bank height, however, absolute values of residuals do show weak correlations to these variables.
* Added Figure 4 showing that residuials are geographically random (Moran's I).
* Added these results to paragraph 1 of section 5.2, "Automated model selection" (line 448 ff.)
* Bank soil types were almost all sand or loamy sand (added to line 118).

The robustness of the model needs to be evaluated further than what is described in figure 4. Looking at Figure 5, there are two sites with apparent high leverage effect on the regression model at the low and high erosion rate extremes. These samples may not have sufficient site density around them to support leaving them in the model. I think we need to see what happens to the model results if both of these sites are removed from the analysis? Do the results stay the same or does the model change significantly?

* We have tested the "best AICc" model according to the reviewer's recommendations. Extreme low and high value removed from analysis and the model selection process was repeated. The resulting model still shows a good fit (R^2^=0.46) and the coefficients are very similar to the original model.
* These outliers are natural outliers (as opposed to measurement errors, e.g.), which is why we left them in originally. This analysis justifies leaving them in.
* Short description of this has been added (line 512 ff.)

The authors already biased the model to an unknown extent by removing 5 on-eroding sites from the model. Therefore, would the model tend to over-predict bank erosion in a future study? Managers will not have erosion history, only status variables from field assessments to drive the model, they won't be able to exclude no-erosion sites as being odd. Following, maybe the authors could check how their model would predict erosion rates for these 5 removed/no-erosion sites? Answering this question might also yield some clues into sensitivity of the model. If the model does not change much after excluding outliers and erosion predictions for the "no erosion" sites make sense, then the justification for the final model would be much stronger.

* Predicted erosion rates for the 5 excluded sites were added as triangles to scatter plots.
* The model does overpredict erosion for the 5 sites, up to 7 cm/year.
* A discussion of this has been added to paragraph 2 of the Discussion section.

p. 19, line 317- so a floodplain n value was used? Why not toe value, I am not clear? What method by Arcement and Schneider (1989) was used? Why floodplain and not channel value? Clear this up with a sentence or two.

* We used Arcement and Schneider's method for channel n, this has been cleared up (line 406).

The application value of this model is limited by the high level of field work required to use it. Therefore, procedures need to be clear and robustness and assumptions clearly spelled out. There are lots of these types of bank erosion indexes out there, so in the revision process, the authors should try to emphasize what makes their model better or an improvement? Can they compare BEHI results to the their combined modeling approach? What percent of improvement in predictions is added their model?

* Addressed in paragraph 4 in the Discussion section of the revised paper.

### Typos

1. equation 18, p 22- "P" is missing from PC%.
2. data were, not "was" on p. 14, below line 227.
3. p. 19, line 318- Arcement not "George" in reference.

* Fixed and updated bibliography.

### Reviewer #3:

#### Overall purpose

The topic presented by the authors builds on a much needed area of work, making stronger linkages between geomorphic science and practitioners using geomorphic tools for river restoration, management and water quality improvement.

#### Approach

The statistical approach is good, it makes sense and is an improvement over the BANCS model.

#### Introduction

There is a good review of other bank erosion equations and BANCS models. However I find there is a lack of geomorphic background relevant to the streambank erosion processes. Specifically the gulf coastal plain is likely very different in terms of the soil type and materials comprising the channel boundaries and so the rates and processes of lateral channel migration may be very different than other regions where BANCS models were developed. The authors should explain how the alluvial setting (or whatever the local setting is) would likely affect the erosion processes and rates. It should also be noted that forested systems are also different in how the vegetation stabilizes or interacts with the bank materials compared to herbaceous plants. See Stanley Trimble (1997) work in the Coon Creek basin of Wisconsin for example. There is some discussion of this in the Discussion section but it would reinforce this topic if it were introduced in this section

* We agree with the reviewer that the local setting is relevant, and in the discussion section we refer to that. However, on page 7, line 117, we have added a brief introduction of this in the hopes that a statement earlier in the manuscript will be beneficial to the reader.
* Although most of the relevant papers concerning the topics were cited, we perhaps did not discuss them well enough in the Introduction.
* Some material that would be better suited for the Introduction was scattered throughout the Methods section. Given the reviewer's comments below, these topics have been moved to the Introduction and discussion added where needed (see paragraph 4 of Introduction).
* Forested and grassy streambanks do differ in stability. We think this is one of the reasons the BANCS model itself did not perform well in the study area. In the present study, all but two of the streambanks were forested, but the BANCS model was developed mostly with grassy banks. We have added a statement about this, which refers to Trimble's 1997 work, on page 5, line 57. We also added a short summary of Trimble's results to the Introduction (paragraph 3).

Secondly the authors make the point that their statistical model may be valuable to stream managers and restorationists (lines 59-62), which is true, yet they don't explain how the two are connected exactly. How would restorationists use the tool? is it enough just to identify stream reaches with greater erosion rates or what other information and judgements would need to be made in order to make this information useful to managers?

* We have added a statement about other types of information needed to make judgements on page 6, line 82 ff.

#### Methods

They have developed some good new tools including the above-ground biomass index (AGB) and the use of an automated model selection tool. However, I think the methods section is a bit long (13 of 32 pages) in proportion to the other parts of the paper - the results section is 6 pages and discussion 4 pages. Some of the literature review on bank erosion models may fit better in the Introductory section or could be reduced.

* The methods section is long but we intended it that way because method development is an essential part of this manuscript. Nevertheless, we agree with the reviewer that some of the literature review on bank erosion models fits better in the introduction and we have moved it.

Other comments: I think the monitoring period of 2 years is short for characterizing long-term erosion rates, maybe there is not too much the authors can do about this now, but it should be noted if the time period of 2014-2016 was a high-rainfall and/or high streamflow period in west Florida. The authors do refer to the need for longer study periods in lines 488-492.

* We have added information about the precipitation and its variability to the first paragraph of the study area section.

Lastly, I think that in the Methods or possibly the Introduction, the authors need to point out that these types of lateral streambank erosion tools only predict gross sediment erosion. They do not attempt to quantify the proportion of that sediment which is deposited on pointbars or floodplains, which is a shortcoming of this type of approach, generally. See Lauer and Parker (2008).

* We have added a statement about this at the end of the first paragraph of the introduction.

#### Results

The first part of the results section describes the root density methodology - this should be transferred to that section. More generally, I think there should be more presentation of the results on the individual factors that were found to have the greatest influence on erosion rates before jumping to the overall model selection in Section 5.3. None of the top five explanatory variables are really presented (AGB, Sb, R\*, BEHI and PC). In particular the AGB is discussed in detail on pages 26-28 but its not assessed separately except for the coefficients shown in Table 4.

* Created supplementary figure (S2) showing correlations of these important variables to erosion rate.
* Discussed individual factors in paragraph 2 of section 5.2 (after equation 17).
* Moved root survey evaluation to Methods.

#### Discussion

Overall I thought this section was good. The questions about tree cover vs. understory cover on the streambanks and how they were found to have opposing effects in this study deserves more attention in this paper. Perhaps if the topic were introduced earlier in the paper it would be more meaningful to the reader.

* This topic was introduced earlier, albeit briefly. It was introduced in results section 5.2 on lines 539 and 540. We have also added a brief mention in the introduction (see paragraph 3 of that section).

#### Conclusions

The authors refer to the uses for restoration planning and sediment loading studies in the first and last paragraphs of this section. It would be helpful if they more explicitly describe how the models are being used in total maximum daily load (TMDL) studies for example and how their model might improve on the BANCS models in this regard.

* We have added a description of the connections between streambank erosion modeling and TMDLs at the end of the first paragraph of the introduction.
* The model(s) from this study improve on the BANCS model in all regards, at least for the study area. The BANCS model performed very poorly in the study area, as mentioned on page 7, line 112, and on page 39 line 643. The references on these lines indicate that the standard BANCS model cannot be used for any application in the study area.

#### References

See notes on Trimble (1997) above and other references I've noted; Lauer, J. W., and G. Parker (2008), Net local removal of floodplain sediment by river meander migration, Geomorphology, 96, 123-149. Also note that there was a BANCS model for North Carolina that may have covered coastal alluvial plains, you should check into that.

* We have included a reference to Trimble (1997) on page 4 and to Lauer and Parker (2007) on page 3.
* The North Carolina study was done on the piedmont and in the mountains of Western North Carolina.

### Other Corrections and Changes

The original description of bank slope (Sb) was incorrect. It was described as bank height divided by distance (m/m). Actually, it was calculated as the sine of the bank angle measured with a clinometer. Section 3.2 and Tables 1 and 2 have been updated to reflect this correction.

Updated figure 5 to show model predictions for sites with negative erosion rates. The y-axis did not extend far enough in the original figure.

Shortened abstract to accomodate new additions.

Two figures have been added as supplementary material and referenced in the text (S1 and S2). Their captions are contained in separate text files.

## Revision 2

### Reviewer #2

The morphologic and vegetation variables assessed in this study have sampling errors (worker and measurement errors) associated with them. There does not seem be any discussion of this aspect. Are all these procedures sound and yield results that are repeatable and precise? One paragraph of discussion on how sampling errors associated with these measurements may affect data variability and model fit is warranted. Focus a bit more on the data rather then just the dataset. There are published errors for some of these methods (comparison of replicate measurements at the same site or reach, but maybe these authors also collected their own QA/QC information including relative difference or CV% of duplicates or replicates or same site comparisons for different workers? Sampling error would represent the limit of modeling precision and may help to evaluate effectiveness for monitoring programs.

One cross-section per site is typically not enough to capture the range of bank variability in a reach or even-sub-reach, how does the present study address potential errors?

* We agree that sampling errors are always a concern when models like these are to be applied by various workers. The errors associated with measuring stream channel geometry have been well documented (see papers cited in paragraph 5 of Discussion), and our models share this weakness. Assessing the impact of measurement error is a larger issue in the field of applied fluvial geomorphology, and should continue to be investigated.
* We thank the reviewer for bringing this issue to our attention. Paragraph 5 has been added to the Discussion section to discuss these points.

One of the contributions of this paper described by the authors is that the reported models can be used to help managers and engineers to assess bank erosion rates. This may be true if the managers have a lot of time or funding on their hands. However, the field data requirements are fairly significant so that realistically agencies may not be able to carry out the work to only explain half of the variance in bank erosion rates at a site. Further, application by other teams really begs the question of work error and how that might affect the results for assessments by other teams (as mentioned above).  
Maybe a paragraph and table listing the variables or bank conditions most commonly associated with stable/low bank erosion banks and those associated with rapidly eroding banks would help bridge this gap and also link back in field geomorphology theory. The idea would be: if you see certain features then you can be reasonably sure that the bank is either eroding or not. If this type of high and low BE rationale cannot be defended then it brings into questions the significance of the models in general to explain channel process and morphology, the main goal of the article.

* We agree that the field work requirements for these models is relatively large, but that is because streambank erosion is a complex process that is specific to individual banks. The reviewer is correct: our data do suggest some factors that may indicate the presence or absence of erosion. We have expanded paragraph 6 of Discussion to address this.
* Paragraph 7 of Discussion links the variables to fluvial geomorphology theory and the large body of previous work on bank erosion. The paragraphs of the Discussion section have been rearranged to make this more clear.

### Reviewer #3

In the abstract, I would suggest adding a concluding statement on the significance of the paper and its importance for stream restoration and management practice. This should be in the abstract because I believe that is the main contribution of this paper - its usefulness for practitioners and improvement on the BANCS process.

* The end of the abstract has been modified as recommended by reviewer #3.

The point I was getting at but didn't make clear enough in the first review is that simply stabilizing streambanks shouldn't qualify as "stream restoration" although many people use the term that way. See the Society for Ecological Restoration website, they describe it as "The process of assisting the recovery of an ecosystem that has been degraded, damaged, or destroyed."

* We fully agree with the reviewer that simply stabilizing streams shouldn’t qualify as stream restoration. We have seen many reaches that were stabilized but not restored. On line 86 we have added the following sentence: “Ideally, geomorphological, hydraulic and ecological data for a local undisturbed system should also be assessed to optimize the restoration in order to help the restored system revert to a pre-disturbance state.”

* Re. the last comment: The reviewer is correct. We apologize for the oversight. No correction needed.
