# Imports ----

library(MASS)
library(leaps)
library(rJava)
library(glmulti)
library(ggplot2)
library(gridExtra)
library(ggthemes)
library(scales)
library(cvTools)
library(DAAG)
library(stargazer)
library(ape)

# Fonts ----
library(extrafont)
Sys.setenv(R_GSCMD = "C:/Program Files/gs/gs9.21/bin/gswin64c.exe")

# Data input ----
alldata = read.csv("matlab-out.csv")
alldata = alldata[1:53, ]
rosgen = read.csv("rosgen.csv")
rosgen = rosgen[1:53, ]
latlon = read.csv("latlon.csv")

minp1 <- function(vec){
  return(vec - min(vec) + 1)
}

alldata <- within(alldata, rm("id", "BasinID"))
alldata$veg_density_weighted <- minp1(alldata$veg_density_weighted)

#alldata$erosion_rate <- alldata$erosion_rate*alldata$bheight
#alldata$U_nb <- minp1(alldata$U_nb)
#alldata[alldata$erosion_rate<0.3,"erosion_rate"] <- NA

logdata = log10(alldata)
valid <- complete.cases(logdata)
logdata <- logdata[valid, ]
latlon <- latlon[valid, ]

outliers <- c(5,35)

REMOVE_OUTLIERS = FALSE;
if(REMOVE_OUTLIERS) {
  logdata <- logdata[-outliers, ]
  latlon <- latlon[-outliers, ]
}

#logdata$behicat = factor(alldata$behiname, c('Very low','Low','Moderate','High','Very high','Extreme'))
#logdata$nbscat = factor(alldata$nbsname, c('Very low','Low','Moderate','High','Very high','Extreme'))
#logdata[logdata$Rc_erf<-10, c('Rc_erf')] <- -10
#logdata$erosion_rate <- log10(minp1(alldata$erosion_rate))
#logdata[logdata$Rc_scaled=="NaN","Rc_scaled"] <- NA

#logdata[logdata$Rc_limaye=="-Inf","Rc_erf"] <- NA

# Data summary table ----

stargazer(alldata,
          summary=TRUE,
          align=FALSE,
          digits = 3,
          float = TRUE,
          single.row=TRUE,
          label=c("tab:summary"),
          out="summary-table.tex")   

# Co-linearity? ----
library(grid)
library(gridExtra)

cor_p1 <- round(cor(alldata$veg_density_weighted, alldata$behi), 2)
lbl_p1 <- paste("italic(R)", " == ", cor_p1)
p1 <- ggplot(alldata, aes(y=behi, x=veg_density_weighted)) +
  geom_point(size=2, color="gray33") +
  theme_minimal() +
  theme(aspect.ratio=1) +
  labs(x=expression(AGB[w]*" ("*kg/m^2*")"), y="BEHI") + 
  annotate("text", x=10, y=50, label=lbl_p1, parse=TRUE)

cor_p2 <- round(cor(alldata$angle_factor, alldata$behi), 2)
lbl_p2 <- paste("italic(R)", " == ", cor_p2)
p2 <- ggplot(alldata, aes(y=behi, x=angle_factor)) +
  geom_point(size=2, color="gray33") +
  theme_minimal() +
  theme(aspect.ratio=1) +
  labs(x=expression(S[b]), y="") +
  annotate("text", x=.55, y=15, label=lbl_p2, parse=TRUE)

cor_p3 <- round(cor(alldata$veg_pcover, alldata$behi), 2)
lbl_p3 <- paste("italic(R)", " == ", cor_p3)
p3 <- ggplot(alldata, aes(y=behi, x=veg_pcover)) +
  geom_point(size=2, color="gray33") +
  theme_minimal() +
  theme(aspect.ratio=1) +
  labs(x="PC (%)", y="") +
  annotate("text", x=40, y=50, label=lbl_p3, parse=TRUE)
ggsave("figs/behi-collinearity.pdf")


grid.arrange(p1, p2, p3, ncol=3)
behiGrob <- arrangeGrob(p1,p2,p3, ncol=3, top="BEHI collinearity?")
ggsave("figs/behi-collinearity.pdf", behiGrob)

# All subsets regression ----

neach <- 50
max <- 16
lps <- regsubsets(erosion_rate ~ .,
                  data = logdata,
                  nbest = neach,
                  nvmax = max,
                  force.in=NULL,
                  force.out=NULL,
                  intercept=TRUE,
                  method = 'exhaustive',
                  really.big=T)
plot(lps, scale = 'bic', main = "BIC")
plot(lps, scale = 'adjr2', main = "Adjusted R^2")
sum <- summary(lps)
terms <- sum$which
rsq <- sum$rsq
adjr2 <- sum$adjr2
cp <- sum$cp
bic <- sum$bic
outmat <- sum$outmat
nterms <- as.numeric(rownames(terms))
lps_df <- data.frame(rsq,adjr2,cp,bic,nterms)
lps_df$bicnorm <- 1-(bic-min(bic))/(10-min(bic))
lps_df$bicnorm[lps_df$bicnorm<0] <- 0

ggplot(lps_df, aes(x=nterms, y=bic, colour=rsq, alpha=0.5)) + 
  geom_point(position=position_jitter(height=0, width=0.0)) +
  theme_classic() +
  scale_x_continuous(breaks=min(nterms):max(nterms)) +
  scale_colour_continuous() +
  labs(x="Number of predictor variables", y="BIC", colour=bquote(R^2)) +
  ggtitle("All models' performance")

crit <- "aicc"
multi <- glmulti(erosion_rate ~ .,
                 data = logdata,
                 intercept=TRUE,
                 level = 1,
                 maxsize = 16,
                 method = "l",
                 crit = crit,
                 plotty = TRUE,
                 report = TRUE,
                 fitfunction = "lm")
nmodels <- multi@nbmods

plot(multi)
plot(multi, type="s")
tmp <- weightable(multi)
tmtmp <- tmp[tmp$aicc <= min(tmp$aicc) + 2, ]
tmp
multi_df <- data.frame(multi@K, multi@crits)
multi_df$multi.K <- multi_df$multi.K - 2
ggplot(data=multi_df, aes(x=multi.K, y=multi.crits, color=multi.crits)) +
  geom_point(size=3, alpha=0.7) +
  theme_minimal() +
  ggtitle(paste(crit, "vs # of predictor variables"))
multi.sum <- summary(multi@objects[[1]])
best <- multi@objects[[1]]
summary(best)

# Output table AIC ----

stargazer(lm(multi@objects[[1]]),
          lm(multi@objects[[2]]),
          lm(multi@objects[[3]]),
          lm(multi@objects[[4]]),
          lm(multi@objects[[5]]),
          align=FALSE,
          omit.stat=c("LL","ser","f","n","adj.rsq"),
          omit.summary.stat=c("sd"),
          keep.stat=c("aic","rsq"),
          report = c("vc*"),
          dep.var.labels=c("Erosion rate"),
          digits = 3,
          float = TRUE,
          ci=FALSE, 
          ci.level=0.90,
          single.row=TRUE,
          label=c("tab:aicres"),
          out="aicbest-table.tex")

# Cross-validation ----

require(DAAG)
folds <- 10
reps <- 5
MSE <- vector(mode="numeric", length=nmodels)
ms_rep <- matrix(nrow=nmodels, ncol=reps)
for(i in 1:nmodels){
  for(j in 1:reps){
    cvout <- CVlm(data=multi@objects[[i]]$model, formula(multi@objects[[i]]), m=folds, plotit=F, printit=T,seed=j)
    ms_rep[i,j] <- attr(cvout, "ms")
  }
  MSE[i] = mean(ms_rep[i,])
}
boxplot(t(ms_rep[1:89, ]))

print("The best model from CV was ranked the following in Information Criterion")
print(which(MSE==min(MSE)))
cvbest <- multi@objects[[which(MSE==min(MSE))]]
summary(cvbest)

cvranks <- sort(MSE,index.return=TRUE)$ix

# Output table CV ----

stargazer(lm(multi@objects[[cvranks[1]]]),
          lm(multi@objects[[cvranks[2]]]),
          lm(multi@objects[[cvranks[3]]]),
          lm(multi@objects[[cvranks[4]]]),
          lm(multi@objects[[cvranks[5]]]),
          align=FALSE,
          omit.stat=c("LL","ser","f","n","adj.rsq"),
          omit.summary.stat=c("sd"),
          keep.stat=c("aic","rsq"),
          report = c("vc*"),
          dep.var.labels=c("Erosion rate"),
          digits = 3,
          float = TRUE,
          ci=FALSE, 
          ci.level=0.90,
          single.row=TRUE,
          label=c("tab:cvres"),
          out="cvbest-table.tex")   

for(j in 1:reps){
  out <- CVlm(data=cvbest$model, formula(cvbest), m=folds, plotit=T, printit=T,seed=j,dots=T)
  ms_rep[j] <- attr(out, "ms")
}
mse <- mean(ms_rep)
summary(cvbest)

# Predict values for sites with zero erosion ----

zerodata <- within(alldata[alldata$erosion_rate<0, ], rm("erosion_rate"))
zerodata <- log10(zerodata)
pz_aic <- predict(best, zerodata)
pz_cv <- predict(cvbest, zerodata)
10^pz_aic
10^pz_cv
plot(10^pz_aic, alldata[alldata$erosion_rate<0, ]$erosion_rate)
plot(10^pz_cv, alldata[alldata$erosion_rate<0, ]$erosion_rate)

# Run the best model excluding the 2 outliers ----

if(!REMOVE_OUTLIERS) {
  summary(best)
  best_no_outliers <- lm(best, logdata[-outliers,])
  summary(best)
  summary(best_no_outliers)
}

# Models using only BEHI ----

simple_lm <- lm(erosion_rate~behi, data=logdata)
simple_lm_no_outliers <- lm(erosion_rate~behi, data=logdata[-outliers, ])
summary(simple_lm)

# Analysis with bank height ----

Yh <- log10(alldata$erosion_rate * alldata$bheight)
Yh <- Yh[complete.cases(Yh)]
bestHeight <- lm(Yh ~ veg_density_weighted + angle_factor + nbs2new + behi + veg_pcover, data=logdata)
summary(bestHeight)

# Scatter Plots ----
model.plot <- function(mdl, color, title, ylabel = "Observed erosion rate cm/yr (log)", xlabel="Predicted erosion rate cm/yr (log)"){
  
  faceplant1 <- function(x) {
    return (c(x[1]*10^.25, x[2]/10^.25))
  }
  faceplant2 <- function(x) {
    return (rep(seq(1,9),5)*rep(10^seq(-1,3), each=9))
  }
  
  data <- mdl$model
  data_zero <- zerodata
  observed <- 10^mdl$model[[1]]
  predicted <- 10^predict(mdl)
  predicted_zero <- 10^predict(mdl, zerodata)
  
  ggplot(data, aes(x = predicted, y = observed, colour="predicted")) + 
    geom_abline(slope = 1, intercept = 0, size=.5, linetype=5) +
    stat_smooth(method=lm, se=TRUE, level=0.95, colour="black", size=0.8) +
    geom_point(size=3) +
    geom_point(data=data_zero, aes(x=predicted_zero, y=10^-1, colour="predicted_zero"), size=4, shape=2, stroke=1.4) + 
    annotation_logticks(base = 10, sides = "bl") +
    theme_minimal() +
    theme(text=(element_text(size=20)), panel.border = element_rect(colour = "black", fill=NA, size=.5)) +
    scale_x_log10(limits=c(10^-1.1, 10^2),
                  breaks=10^seq(-1,3),
                  minor_breaks=trans_breaks(faceplant1, faceplant2, n=45)) +
    scale_y_log10(limits=c(10^-1.1, 10^2.2),
                  breaks=10^seq(-1,3),
                  minor_breaks=trans_breaks(faceplant1, faceplant2, n=45)) +
    labs(x=xlabel, y=ylabel) +
    annotate("text", x=10^-.5, y=10^1.9, label=paste("italic(R)^2", " == ", round(summary(mdl)$r.squared, 2)), parse=TRUE, size=7) +
    annotate("text", x=10^1.8, y=10^1.5, label="1:1", size=6, colour="gray33") +
    ggtitle(title) +
    scale_colour_manual(name=element_blank(), 
                        breaks = c("predicted", "predicted_zero"),
                        values = c(color, "black"),
                        labels = c("Predicted", "Predicted (for Y \u2264 0)"),
                        guide = guide_legend(override.aes = list(
                          linetype = c(NA,NA),
                          shape = c(16,2),
                          size=c(3,4),
                          stroke=c(NA,1.4)))
    ) +
    theme(legend.justification=c(1,0), legend.position=c(0.9,0.1), legend.box.background=element_rect(fill="white")) +
    ggsave(filename=paste(title, ".pdf"), device=cairo_pdf, path="figs/", width=8, height=6)
}

model.plot(best, "red", "Best AICc model")
model.plot(best_no_outliers, "orange", "Best AICc model (outliers removed)")
model.plot(cvbest, "blue", "Best CV model")
model.plot(simple_lm, "gray33", "BEHI model")

# Arrow Plots ----

y <- coef(cvbest)[-1]
x <- seq_along(y)
ci = confint(cvbest, level=0.9)[-1,]
xlim = range(x) + c(-0.5,0.2)
ylim = range(ci)
ylim = ylim + 0.1*c(-1,+1)*diff(ylim) # extend it a little
ylim[1] <-  min(ylim[1],0)
ylab = bquote(hat(beta))
xlab = "coefficient"
par(mar=c(4.5,5,1,1), las=1)
plot(x=y, y=x, pch=16, xlim=ylim, ylim=xlim, xlab=ylab, ylab="", yaxt="n", bty="n")
axis(2, at=x, labels=names(y), tick=FALSE)
abline(v=0, lty=3)
arrows(ci[,1],x,ci[,2],x, code=3, angle=90, length=0.05)

# Residual analysis ----

ggQQ <- function(LM, title, color){
  y <- quantile(LM$resid[!is.na(LM$resid)], c(0.25, 0.75))
  x <- qnorm(c(0.25, 0.75))
  slope <- diff(y)/diff(x)
  int <- y[1L] - slope * x[1L]
  p <- ggplot(LM, aes(sample=.resid)) +
    stat_qq(alpha = 0.5, size=3,colour=color) +
    geom_abline(slope = slope, intercept = int, color="black") +
    theme_minimal() +
    theme(text=(element_text(size=14))) +
    labs(x='Theoretical Quantiles', y=ifelse(color=="red", 'Sample Quantiles', "")) +
    scale_x_continuous( limits=c(-3.6, 3.6)) +
    scale_y_continuous( limits=c(-1.2, 1.2)) +
    ggtitle(title) +
    theme(aspect.ratio=1) 
  
  return(p)
}

res <- resid(best)
res.cm <- (10^best$model$erosion_rate) - 10^predict(best)
qqnorm(res)
qq1 <- ggQQ(best, "Best AICc", "red")
qq2 <- ggQQ(cvbest, "Best CV", "blue")
qq3 <- ggQQ(bestHeight, "Best AICc Yh", "purple")

grid.arrange(qq1, qq2, qq3, ncol = 3, nrow = 1)
qqGrob <- arrangeGrob(qq1,qq2,qq3,ncol=3,nrow=1)
ggsave(filename="figs/qq-plots.pdf", plot=qqGrob, device=cairo_pdf, width=8, height=6)

ggCorPlot <- function(x,y,color="black",labx,laby,labelx=0,labely=0.75, stat_smooth = FALSE, lm=TRUE) {
  r <- cor(x, y)
  q <- qplot(x, y, color=color) +
    scale_color_manual(values=c(color)) +
    labs(x=labx, y=laby) + 
    annotate("text", x=labelx,y=labely, label=paste("R==",round(r,3)), parse=TRUE) +
    theme_minimal() +
    theme(legend.position="none") +
    theme(text=(element_text(size=14)), panel.border = element_rect(colour = "grey33", fill=NA, size=.5))
  if (stat_smooth) 
  {
    q <-  q + geom_smooth(size=0) + stat_smooth(geom="line",alpha=0.5, size=1.2) 
  }
  if(lm)
  {
    q <- q + stat_smooth(geom="line",method="lm", size=1, alpha=0.5, size=1.2) 
  }
  return(q)
}

q1 <- ggCorPlot(x=logdata$drainage_A_sqkm, y=res, labx=expression(DA), laby="Residuals")
q2 <- ggCorPlot(x=logdata$drainage_A_sqkm, y=abs(res), color="red", labx=expression(A[d]), laby="Absolute Residuals")
q3 <- ggCorPlot(x=logdata$bankfullwidth, y=res, labx=expression(B), laby="Residuals", labelx=1.4)
q4 <- ggCorPlot(x=logdata$bankfullwidth, y=abs(res), color="red", labx=expression(B), laby="Absolute Residuals", labelx=1.4)
q5 <- ggCorPlot(x=logdata$psand, y=res, labx=expression(F[sd]), laby="Residuals", labelx=1.6)
q6 <- ggCorPlot(x=logdata$psand, y=abs(res), color="red", labx=expression(F[sd]), laby="Absolute Residuals", labelx=1.6)
q7 <- ggCorPlot(x=logdata$pclay, y=res, labx=expression(F[cy]), laby="Residuals")
q8 <- ggCorPlot(x=logdata$pclay, y=abs(res), color="red", labx=expression(F[cy]), laby="Absolute Residuals")
q9 <- ggCorPlot(x=logdata$bheight, y=res, labx=expression(H[b]), laby="Residuals")
q10 <- ggCorPlot(x=logdata$bheight, y=abs(res), color="red", labx=expression(H[b]), laby="Absolute Residuals")
q11 <- ggCorPlot(x=logdata$erosion_rate, y=res, labx="Erosion rate", laby="Residuals")
q12 <- ggCorPlot(x=logdata$erosion_rate, y=abs(res), color="red", labx="Erosion rate", laby="Absolute Residuals")

rqGrob <- arrangeGrob(q1,q3,q2,q4,q5,q7,q6,q8,q9,q11,q10,q12, nrow=3, ncol=4)
ggsave(filename="figs/residual-correlations.pdf", plot=rqGrob, width=10, height=8,  useDingbats=FALSE)

# Moran's I for residuals----
# https://stats.idre.ucla.edu/r/faq/how-can-i-calculate-morans-i-in-r/

#calc distance matrix
res.dists <- as.matrix(dist(cbind(latlon$Lon, latlon$Lat)))

# calc inverse distance weights
res.dists.inv <- 1/res.dists
diag(res.dists.inv) <- 0
moranI <- Moran.I(res, res.dists.inv)
moranI.cm <- Moran.I(res.cm, res.dists.inv)

library(ggmap)
library(maps)
library(mapdata)

area <- borders("worldHires", regions="usa", 
                 xlim=c(min(latlon$Lon)-.1,max(latlon$Lon)+.1), 
                 ylim=c(min(latlon$Lat)-.2,max(latlon$Lat)+.1), 
                 colour="gray33", fill="gray92")

ewbrks <- seq(-88,-84,1)
nsbrks <- seq(29,32,1)
ewlbls <- unlist(lapply(ewbrks, function(x) ifelse(x < 0, parse(text=paste0(-x,"^o", "*W")), ifelse(x > 0, parse(text=paste0(x,"^o", "*E")),x))))
nslbls <- unlist(lapply(nsbrks, function(x) ifelse(x < 0, parse(text=paste0(-x,"^o", "*S")), ifelse(x > 0, parse(text=paste0(x,"^o", "*N")),x))))

map1 <- ggplot() + 
  area +
  geom_vline(xintercept=seq(-88, -84, by=0.5), colour="gray75", size=0.4, linetype=2) +
  geom_hline(yintercept=seq(29, 32, by=0.5), colour="gray75", size=0.4, linetype=2) +
  geom_vline(xintercept=seq(-88, -84, by=1), colour="gray66") +
  geom_hline(yintercept=seq(29, 32, by=1), colour="gray66") +
  geom_point(data = latlon, aes(x = Lon, y = Lat, size=abs(res), colour=ifelse(sign(res>0),"Positive","Negative")), shape=1, stroke=1) +
  coord_fixed(xlim=c(min(latlon$Lon-.1), max(latlon$Lon+.1)), ylim=c(min(latlon$Lat)-.2,max(latlon$Lat)+.1)) + 
  scale_size_manual() +
  theme_minimal() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  theme(text=(element_text(size=14)), panel.border = element_rect(colour = "black", fill=NA, size=.5)) +
  theme(legend.justification=c(1,1), 
        legend.position=c(1,1),
        legend.direction="horizontal", 
        legend.box="vertical", 
        legend.spacing.y = unit(-0.5, "cm"),
        legend.box.background = element_rect(fill = "white"),
        legend.text=element_text(color="gray33")) +
  theme(axis.title.x=element_blank(),
        axis.title.y=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_text(angle=90,hjust=0.5)) +
  guides(size = guide_legend(title.position="top", title.hjust = 0.5, order=1),
         colour = guide_legend(order=2)) +
  labs(x=element_blank(), y=element_blank()) +
  scale_size(name="Residuals (log)", range=c(0,12), limits=c(0,1.5), breaks=c(0.01, 0.1, 0.5, 1.2)) +
  scale_color_hue(name="", h=c(0,250)) +
  scale_y_continuous(breaks=nsbrks, labels=nslbls) +
  scale_x_continuous(breaks=ewbrks, labels=ewlbls) +
  annotate("text", x=-86.8, y=30.2, 
           label=paste("Moran's I = ", round(moranI$observed, 3)," ",
                           "(",round(moranI$expected, 3),"\u00B1", round(moranI$sd, 3),"), ",
                           "p = ", round(moranI$p.value,2), sep=""),
           size=4) +
  annotate("text", x=-87.7, y=31.55, label="A", size=6) 

map2 <- ggplot() + 
  area +
  geom_vline(xintercept=seq(-88, -84, by=0.5), colour="gray75", size=0.4, linetype=2) +
  geom_hline(yintercept=seq(29, 32, by=0.5), colour="gray75", size=0.4, linetype=2) +
  geom_vline(xintercept=seq(-88, -84, by=1), colour="gray66") +
  geom_hline(yintercept=seq(29, 32, by=1), colour="gray66") +
  geom_point(data = latlon, aes(x = Lon, y = Lat, size=abs(res.cm), colour=ifelse(sign(res.cm>0),"Positive","Negative")), shape=1, stroke=1) +
  coord_fixed(xlim=c(min(latlon$Lon-.1), max(latlon$Lon+.1)), ylim=c(min(latlon$Lat)-.2,max(latlon$Lat)+.1)) + 
  scale_size_manual() +
  theme_minimal() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  theme(text=(element_text(size=14)), panel.border = element_rect(colour = "black", fill=NA, size=.5)) +
  theme(legend.justification=c(1,1), 
        legend.position=c(1,1),
        legend.direction="horizontal", 
        legend.box="vertical",
        legend.box.background = element_rect(fill = "white"),
        legend.spacing.y = unit(-0.5, "cm"),
        legend.text=element_text(color="gray33")) +
  theme(axis.title.y=element_blank(), 
        axis.title.x=element_blank(),
        axis.ticks.x=element_blank(),
        axis.text.y=element_text(angle=90,hjust=0.5)) +
  guides(size = guide_legend(title.position="top", title.hjust = 0.5, order=1),
         colour = guide_legend(order=2)) +
  scale_size(name="Residuals (cm/yr)", range=c(0,14), limits=c(0,150), breaks=c(1, 10, 50, 120), labels=c(1, 10, 50, "120\ncm/yr")) +
  scale_color_hue("", h=c(0,250)) +
  scale_y_continuous(breaks=nsbrks, labels=nslbls) +
  scale_x_continuous(breaks=ewbrks, labels=ewlbls) +
  annotate("text", x=-86.8, y=30.2, size=4,
           label=paste("Moran's I = ", round(moranI.cm$observed, 3)," ",
                       "(",round(moranI.cm$expected, 3),"\u00B1", round(moranI.cm$sd, 3),"), ",
                       "p = ", round(moranI.cm$p.value,2), sep="")) +
  annotate("text", x=-87.7, y=31.55, label="B", size=6)
grid.arrange(map1, map2, ncol=1, nrow=2)
grob <- arrangeGrob(map1, map2, ncol=1, nrow=2)
ggsave(filename="map.pdf", plot=grob, device=cairo_pdf, path="figs/", width=6.5, height=6)

# Individual variables ----

v1 <- ggCorPlot(x=logdata$behi, y=logdata$erosion_rate, labx=expression(BEHI[]), laby="Erosion rate", labelx=0.9, labely=1.75, stat_smooth=TRUE, lm=FALSE)
v2 <- ggCorPlot(x=logdata$veg_pcover, y=logdata$erosion_rate, labx=expression(PC[]), laby="Erosion rate", labelx=1.3, labely=1.75, stat_smooth=TRUE, lm=FALSE)

v3 <- ggCorPlot(x=logdata$nbs2new, y=logdata$erosion_rate, labx=expression(R["*"]), laby="Erosion rate", labelx=1, labely=1.75, stat_smooth=TRUE, lm=FALSE)
v4 <- ggCorPlot(x=logdata$angle_factor, y=logdata$erosion_rate, labx=expression(S[b]), laby="Erosion rate", labelx=-0.4, labely=1.75, stat_smooth=TRUE, lm=FALSE)
v5 <- ggCorPlot(x=logdata$veg_density_weighted, y=logdata$erosion_rate, labx=expression(AGB[w]), laby="Erosion rate", labelx=0.5, labely=1.75, stat_smooth=TRUE, lm=FALSE)
v6 <- ggCorPlot(x=logdata$bankfullwidth, y=logdata$nbs2new, labx=expression(B[]), laby=expression(R["*"]), labelx=0.75, labely=1.7, stat_smooth=TRUE, lm=FALSE, color="blue")

vGrob <- arrangeGrob(v1, v2, v3, v4, v5, v6, nrow=2, ncol=3)
ggsave(filename="figs/important-variables.pdf", plot=vGrob, width=10, height=8,  useDingbats=FALSE)
